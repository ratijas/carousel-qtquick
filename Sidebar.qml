import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.20 as Kirigami

RowLayout {
    Layout.fillWidth: false
    Layout.fillHeight: true

    spacing: 0

    Rectangle {
        Layout.fillWidth: true
        Layout.fillHeight: true

        implicitWidth: sidebarTop.implicitWidth
        implicitHeight: sidebarTop.implicitHeight + sidebarBottom.implicitHeight

        color: "white"

        Image {
            id: sidebarTop
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: -2
            source: Qt.resolvedUrl("./assets/sidebar/top.png")
        }

        Image {
            id: sidebarBottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            source: Qt.resolvedUrl("./assets/sidebar/bottom.png")
        }
    }

    Kirigami.Separator {
        Layout.fillHeight: true
    }
}
