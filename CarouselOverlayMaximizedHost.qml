import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15

import org.kde.kirigami 2.20 as Kirigami

CarouselAbstractMaximizedHost {
    id: root

    function open() {
        refreshOrigin();

        const item = carouselModel.get(currentIndex);
        delegate.isAnimated = item.isAnimated;
        delegate.image = item.image;

        popup.open();
    }

    function close() {
        popup.close();
    }

    function refreshOrigin() {
        const item = inlineView.itemAtIndex(currentIndex);
        popup.originItem = item;
        if (item) {
            const rect = item.mapToItem(carouselContainer, 0, 0, item.width, item.height);
            popup.originRect = rect;
        }
    }

    readonly property QQC2.Popup __popup: QQC2.Popup {
        id: popup

        property Item originItem
        property rect originRect

        property real transitionProgress

        anchors.centerIn: parent
        parent: inlineView?.QQC2.Overlay.overlay ?? null

        closePolicy: QQC2.Popup.CloseOnEscape
        visible: false
        focus: true
        modal: true
        dim: true

        width: inlineView?.Window.window?.width ?? 0
        height: inlineView?.Window.window?.height ?? 0

        topMargin: 0
        leftMargin: 0
        rightMargin: 0
        bottomMargin: 0

        topPadding: 0
        leftPadding: 0
        rightPadding: 0
        bottomPadding: 0

        contentItem: Item {

            Kirigami.Theme.inherit: true
            Kirigami.Theme.textColor: "white"

            TapHandler {
                onTapped: popup.close();
            }

            WheelHandler {
                acceptedModifiers: Qt.ControlModifier
                target: delegate
                property: "scale"
            }

            Item {
                id: carouselContainer

                anchors {
                    fill: parent
                    margins: Kirigami.Units.gridUnit * 7
                }

                CarouselDelegate {
                    id: delegate

                    readonly property point originBottomRight: Qt.point(
                        popup.originRect.x + popup.originRect.width,
                        popup.originRect.y + popup.originRect.height,
                    )

                    readonly property size targetSize: {
                        if (implicitWidth < parent.width && implicitHeight < parent.height) {
                            return Qt.size(
                                parent.width + leftPadding + rightPadding,
                                parent.height + topPadding + bottomPadding,
                            );
                        }
                        const preferredWidth = implicitHeight * preferredRatio;
                        const preferredHeight = implicitWidth / preferredRatio;

                        let scale = 1.0;
                        if (preferredWidth > parent.width) {
                            scale = Math.min(scale, parent.width / preferredWidth);
                        }
                        if (preferredHeight > parent.height) {
                            scale = Math.min(scale, parent.height / preferredHeight);
                        }
                        return Qt.size(
                            Math.round(preferredWidth * scale) + leftPadding + rightPadding,
                            Math.round(preferredHeight * scale) + topPadding + bottomPadding,
                        );
                    }

                    readonly property point targetTopLeft: Qt.point(
                        Math.round((parent.width - targetSize.width) / 2),
                        Math.round((parent.height - targetSize.height) / 2),
                    )

                    readonly property point targetBottomRight: Qt.point(
                        targetTopLeft.x + targetSize.width,
                        targetTopLeft.y + targetSize.height,
                    )

                    function defaultX() {
                        if (popup.originItem) {
                            return root.lerp(popup.originRect.x, targetTopLeft.x, popup.transitionProgress);
                        } else {
                            return targetTopLeft.x;
                        }
                    }

                    function defaultY() {
                        if (popup.originItem) {
                            return root.lerp(popup.originRect.y, targetTopLeft.y, popup.transitionProgress);
                        } else {
                            return targetTopLeft.y;
                        }
                    }

                    function defaultWidth() {
                        // body...
                    }

                    x: defaultX()
                    y: defaultY()

                    width: root.lerp(originBottomRight.x, targetBottomRight.x + leftPadding + rightPadding, popup.transitionProgress) - x
                    height: root.lerp(originBottomRight.y, targetBottomRight.y + topPadding + bottomPadding, popup.transitionProgress) - y

                    dim: false
                    loadFull: true

                    index: root.currentIndex
                    image: ""
                    isAnimated: false
                    carouselModel: root.carouselModel

                    onClicked: root.close()

                    // WheelHandler integration
                    onScaleChanged: {
                        if (scale < 1.0) {
                            scale = 1.0;
                            x = Qt.binding(() => defaultX())
                            y = Qt.binding(() => defaultY())
                        }
                    }
                }
            }

            BestestRoundButton {
                LayoutMirroring.enabled: root.mirrored
                anchors.leftMargin: root.lerp(-width - Kirigami.Units.gridUnit, 0, popup.transitionProgress)
                view: root.inlineView
                role: Qt.AlignLeading
                extraEdgePadding: Kirigami.Units.gridUnit * 3

                onClicked: {
                    view?.decrementCurrentIndex();
                }
            }

            BestestRoundButton {
                LayoutMirroring.enabled: root.mirrored
                anchors.rightMargin: root.lerp(-width - Kirigami.Units.gridUnit, 0, popup.transitionProgress)
                view: root.inlineView
                role: Qt.AlignTrailing
                extraEdgePadding: Kirigami.Units.gridUnit * 3

                onClicked: {
                    view?.incrementCurrentIndex();
                }
            }

            BestestPageIndicator {
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: root.lerp(-height - Kirigami.Units.gridUnit, 0, popup.transitionProgress)
                }
                bottomPadding: Kirigami.Units.gridUnit
                count: root.carouselModel.count
                currentIndex: 2
                interactive: true
            }
        }

        Binding {
            target: popup.originItem
            property: "visible"
            value: false
            when: popup.visible
            restoreMode: Binding.RestoreBindingOrValue
        }

        enter: Transition {
            NumberAnimation {
                target: popup.background
                property: "opacity"
                from: 0.0
                to: 1.0
                easing.type: Easing.OutCubic
                duration: Kirigami.Units.longDuration
            }
            NumberAnimation {
                target: popup
                property: "transitionProgress"
                from: 0.0
                to: 1.0
                easing.type: Easing.OutCubic
                duration: Kirigami.Units.longDuration
            }
        }

        exit: Transition {
            NumberAnimation {
                target: popup.background
                property: "opacity"
                from: 1.0
                to: 0.0
                easing.type: Easing.OutCubic
                duration: Kirigami.Units.longDuration
            }
            NumberAnimation {
                target: popup
                property: "transitionProgress"
                from: 1.0
                to: 0.0
                easing.type: Easing.OutCubic
                duration: Kirigami.Units.longDuration
            }
        }

        background: Item {
            Rectangle {
                anchors.fill: parent
                color: "#262828"
            }
        }

        QQC2.Overlay.modal: Item { }

        onClosed: root.closed()

        onAboutToHide: {
            refreshOrigin();
        }
    }
}
