import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15

import org.kde.kirigami 2.20 as Kirigami

QQC2.Control {
    id: root

    required property CarouselModel carouselModel

    property alias displayMarginBeginning: view.displayMarginBeginning
    property alias displayMarginEnd: view.displayMarginEnd

    padding: 0
    topPadding: undefined
    leftPadding: undefined
    rightPadding: undefined
    bottomPadding: undefined
    Layout.fillWidth: true

    focusPolicy: Qt.StrongFocus
    Keys.forwardTo: view

    contentItem: ColumnLayout {
        spacing: Kirigami.Units.largeSpacing * 3

        LayoutMirroring.enabled: root.mirrored

        ListView {
            id: view

            Layout.fillWidth: true
            Layout.preferredHeight: 280

            keyNavigationEnabled: true
            interactive: true

            pixelAligned: true
            orientation: ListView.Horizontal
            snapMode: ListView.SnapToItem
            highlightRangeMode: ListView.StrictlyEnforceRange

            preferredHighlightBegin: currentItem ? Math.round((view.width - currentItem.width) / 2) : 0
            preferredHighlightEnd: currentItem ? preferredHighlightBegin + currentItem.width : 0

            // header: Item {
            //     implicitWidth: view.currentItem ? Math.round((view.width - view.itemAtIndex(0).width) / 2) : 0
            //     implicitHeight: view.height
            // }
            // headerPositioning: ListView.InlineHeader

            // footer: Item {
            //     implicitWidth: view.currentItem ? Math.round((view.width - view.itemAtIndex(view.count - 1).width) / 2) : 0
            //     implicitHeight: view.height
            // }
            // footerPositioning: ListView.InlineHeader

            onPreferredHighlightBeginChanged: {
                print("HB", preferredHighlightBegin)
            }
            onPreferredHighlightEndChanged: {
                print("HE", preferredHighlightEnd)
            }

            highlightMoveDuration: Kirigami.Units.longDuration
            highlightResizeDuration: Kirigami.Units.longDuration
            // highlightResizeVelocity: 10

            cacheBuffer: 10000

            currentIndex: 0
            model: root.carouselModel
            delegate: CarouselDelegate {
                carouselModel: root.carouselModel
                onMaximize: maximizedHostController.open(index)
            }

            onCurrentIndexChanged: {
                print("CIdx", currentIndex);
                pageIndicator.currentIndex = currentIndex;
                positionNewCurrentItemAtCenter();
            }

            onFlickStarted: {
                contentXAnimation.stop();
            }

            onFlickEnded: {
                positionNewCurrentItemAtCenter();
            }

            onContentXChanged: print("CX", contentX)

            onAtXBeginningChanged: print("AXB", atXBeginning)

            function positionNewCurrentItemAtCenter() {
                // DEBUG
                return;

                // find new snapping offset
                contentXAnimation.from = contentX;
                const centerX = availableWidth / 2;
                let indexAtCenter = indexAt(centerX, height / 2);
                print("I", indexAtCenter)
                if (indexAtCenter === -1) {
                    if (contentX < 0) {
                        indexAtCenter = 0;
                    } else {
                        indexAtCenter = count - 1;
                    }
                }
                const itemAtCenter = itemAtIndex(indexAtCenter);
                if (itemAtCenter === null) {
                    return;
                }
                print("AAA", indexAtCenter, itemAtCenter);
                const newContentX = Math.round(itemAtCenter.x + (availableWidth - itemAtCenter.width) / 2)
                currentIndex = indexAtCenter;
                contentXAnimation.to = newContentX;
                contentXAnimation.start();
            }

            NumberAnimation on contentX {
                id: contentXAnimation

                running: false

                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutCubic
            }

            BestestRoundButton {
                LayoutMirroring.enabled: root.mirrored
                anchors.left: parent.left
                height: parent.height
                view: view
                role: Qt.AlignLeading

                onClicked: {
                    view.decrementCurrentIndex();
                }
            }

            QQC2.Button {
                anchors.centerIn: parent
                visible: false

                text: "Refresh beginning/end"
                onClicked: {

                    view.positionViewAtIndex(view.count - 1, ListView.Beginning)

                    // view.atXEndChanged();
                    // view.atXBeginningChanged();
                    // view.atYEndChanged();
                    // view.atYBeginningChanged();
                }
            }

            BestestRoundButton {
                LayoutMirroring.enabled: root.mirrored
                anchors.right: parent.right
                height: parent.height
                view: view
                role: Qt.AlignTrailing

                onClicked: {
                    view.incrementCurrentIndex();
                }
            }
        }

        BestestPageIndicator {
            id: pageIndicator

            Layout.fillWidth: true

            focusPolicy: Qt.NoFocus

            interactive: true
            count: view.count

            onCurrentIndexChanged: {
                view.currentIndex = currentIndex
            }
            Component.onCompleted: {
                currentIndex = view.currentIndex;
            }
        }
    }

    CarouselMaximizedHostController {
        id: maximizedHostController

        carouselModel: root.carouselModel
        inlineView: view

        preferredHost: CarouselMaximizedHostController.Host.Overlay
    }

    Component.onCompleted: {
        // view.currentIndex = 2;
    }
}
