import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15
import QtQml.Models 2.15

import org.kde.kirigami 2.20 as Kirigami

QQC2.Pane {
    id: root

    width: 1160
    height: 800
    padding: 0

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    CarouselModel {
        id: carouselModel

        Component.onCompleted: {
            for (const [isAnimated, image] of [
                [false, "steam-cat.jpg"],
                [false, "wide.png"],
                [false, "tall.png"],
                [false, "org.kde.krita-f17ea004ef26633740e0ca2b4059de7e.png"],
                [false, "org.kde.krita-151de16db373d47d95ff12f50f5b7338.png"],
                [false, "org.kde.krita-4bf2db161d624cd9169ecb54740fa4ad.png"],
                [false, "org.kde.krita-2f1a3f82a2bfd8e46ec6b38096c3e298.png"],
            ]) {
                append({ isAnimated, image });
            }
        }
    }

    RowLayout {
        anchors.fill: parent
        spacing: 0

        Sidebar {}

        AppPage {
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true

            carouselModel: carouselModel
        }
    }

    Connections {
        target: root.Window

        function onActiveFocusItemChanged() {
            print("ITEM", root.Window.activeFocusItem);
        }
    }
}
