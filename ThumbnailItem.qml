import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15

import org.kde.kirigami 2.20 as Kirigami

Item {
    id: root

    implicitHeight: animated.implicitHeight
    implicitWidth: animated.implicitWidth

    Column {
        AnimatedImage {
            id: animated

            cache: false
            source: "http://127.0.0.1:8001/assets/gif/cartoon-toons.gif"

            onImplicitWidthChanged: {
                print("I", implicitWidth)
                root.Window.window.width = implicitWidth
                root.Window.window.height = implicitHeight * 2
            }
            onImplicitHeightChanged: {
                print("I", implicitHeight)
                root.Window.window.width = implicitWidth
                root.Window.window.height = implicitHeight * 2
            }
            onStatusChanged: {
                print("S", status)
                animated2.source = source;
            }

            // z: 1
            // anchors.centerIn: parent

            QQC2.ProgressBar {
                anchors.centerIn: parent
                value: 0.5
            }
        }

        AnimatedImage {
            id: animated2
            cache: false
        }
    }
}
