import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15

import org.kde.kirigami 2.20 as Kirigami

QtObject {
    property CarouselModel carouselModel
    property ListView inlineView

    property int currentIndex: -1

    function open() {}

    function close() {}

    function lerp(a: real, b: real, t: real): real {
        return /*Math.round*/(
            a + t * (b - a)
        );
    }

    signal closed()

    signal switchMode()
}
