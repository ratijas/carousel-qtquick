import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Templates 2.15 as T
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15
import QtQml.Models 2.15

import org.kde.kirigami 2.20 as Kirigami

QQC2.Page {
    id: root

    required property CarouselModel carouselModel

    leftPadding: Kirigami.Units.gridUnit * 3
    rightPadding: Kirigami.Units.gridUnit * 3

    header: QQC2.ToolBar {
        RowLayout {
            anchors.fill: parent
            spacing: 0
            QQC2.ToolButton {
                icon.name: "go-previous"
            }
            Item {
                Layout.fillWidth: true
            }
            Kirigami.ActionToolBar {
                Layout.fillWidth: true
                Layout.fillHeight: true
                alignment: Qt.AlignRight
                actions: [
                    Kirigami.Action {
                        text: "Remove"
                        icon.name: "delete"
                    },
                    Kirigami.Action {
                        text: "Launch"
                        icon.name: "media-playback-start"
                    },
                    Kirigami.Action {
                        text: "From Flathub"
                        icon.name: "flatpak-discover"
                        Kirigami.Action {
                            text: "Flathub"
                            icon.name: "flatpak-discover"
                        }
                    }
                ]
            }
        }
    }

    ColumnLayout {
        id: rootLayout

        anchors.fill: parent
        spacing: 0

        Item {
            Layout.preferredHeight: Kirigami.Units.gridUnit * 5
            Layout.fillWidth: true

            Kirigami.Heading {
                text: "Screenshots"
                level: 1
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }

        CarouselInlineView {
            carouselModel: root.carouselModel

            displayMarginBeginning: root.leftPadding
            displayMarginEnd: root.rightPadding
        }

        ColumnLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.topMargin: Kirigami.Units.gridUnit * 2

            spacing: Kirigami.Units.largeSpacing

            Kirigami.SelectableLabel {
                Layout.fillWidth: true
                text: "Digital Painting, Creative Freedom"
                wrapMode: Text.Wrap

                // Match `level: 2` in Kirigami.Heading
                font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1.2
                // Match `type: Kirigami.Heading.Type.Primary` in Kirigami.Heading
                font.weight: Font.DemiBold

                Accessible.role: Accessible.Heading
            }

            Kirigami.SelectableLabel {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                textFormat: TextEdit.RichText
                text: `
<p>Krita is the full-featured digital art studio.</p>
<p>It is perfect for sketching and painting, and presents an end–to–end solution for creating digital painting files from scratch by masters.</p>
<p>Krita is a great choice for creating concept art, comics, textures for rendering and matte paintings. Krita supports many colorspaces like RGB and CMYK at 8 and 16 bits integer channels, as well as 16 and 32 bits floating point channels.</p>
<p>Have fun painting with the advanced brush engines, amazing filters and many handy features that make Krita enormously productive.</p>
`
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        parent: root.background

        z: 1

        gradient: Gradient {
            orientation: Gradient.Vertical
            GradientStop {
                position: 0
                color: "#F9EEF5"
            }
            GradientStop {
                position: 1
                color: Qt.rgba(1, 1, 1, 0)
            }
        }
    }
}
