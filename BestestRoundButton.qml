import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.20 as Kirigami

QQC2.RoundButton {
    id: controlRoot

    required property ListView view

    property int role: Qt.AlignLeading

    readonly property int effectiveRole: mirrored ? (role === Qt.AlignLeading ? Qt.AlignTrailing : Qt.AlignLeading) : role

    property real extraEdgePadding: 0

    anchors {
        left: role === Qt.AlignLeading ? parent?.left : undefined
        right: role === Qt.AlignTrailing ? parent?.right : undefined
    }

    height: parent?.height ?? undefined

    leftInset: effectiveRole === Qt.AlignLeading ? extraEdgePadding : 0
    rightInset: effectiveRole === Qt.AlignTrailing ? extraEdgePadding : 0

    leftPadding: 6 + leftInset
    rightPadding: 6 + rightInset

    icon.name: effectiveRole === Qt.AlignLeading
        ? "arrow-left" : "arrow-right"
    icon.width: Kirigami.Units.iconSizes.smallMedium
    icon.height: Kirigami.Units.iconSizes.smallMedium

    text: effectiveRole === Qt.AlignLeading
        ? "Previous Screenshot" : "Next Screenshot"
        // ? i18n("Previous Screenshot") : i18n("Next Screenshot")

    focusPolicy: Qt.NoFocus
    display: QQC2.AbstractButton.IconOnly

    // enabled: effectiveRole === Qt.AlignLeading
    //     ? !view.atXBeginning
    //     : !view.atXEnd

    enabled: effectiveRole === Qt.AlignLeading
        ? view.currentIndex !== 0
        : view.currentIndex !== view.count - 1

    opacity: enabled ? 1 : 0

    Behavior on opacity {
        NumberAnimation {
            duration: Kirigami.Units.longDuration
            easing.type: Easing.OutCubic
        }
    }

    transform: Translate {
        x: controlRoot.enabled ? 0
            : Kirigami.Units.gridUnit * 2 * (controlRoot.effectiveRole === Qt.AlignLeading ? -1 : 1)

        Behavior on x {
            NumberAnimation {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.OutCubic
            }
        }
    }

    background: Item {
        Rectangle {
            id: backgroundImpl

            property color borderColor: Qt.tint(controlRoot.palette.buttonText, Qt.rgba(color.r, color.g, color.b, 0.7))

            anchors.centerIn: parent

            width: Math.min(parent.width, parent.height)
            height: width
            radius: width

            border.width: 1
            border.color: borderColor

            // Put tooltip here, so it doesn't show up all the way on the top of the gallery
            QQC2.ToolTip.text: controlRoot.text
            QQC2.ToolTip.visible: controlRoot.enabled && (Kirigami.Settings.isMobile ? controlRoot.pressed : controlRoot.hovered)
            QQC2.ToolTip.delay: Kirigami.Units.toolTipDelay
        }
    }

    onClicked: {
        // Don't want it to get in the way of vieweing screenshots.
        backgroundImpl.QQC2.ToolTip.hide();
    }
}
