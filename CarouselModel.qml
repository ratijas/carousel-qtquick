import QtQml 2.15
import QtQml.Models 2.15

// roles:
// - "image": string, filename
// - "isAnimated": bool, whether the image is an animated gif type
ListModel {
    id: screenshotsModel

    readonly property string assetsBaseDir: "http://127.0.0.1:8001/assets"
    // readonly property string assetsBaseDir: "./assets"

    function resolveThumbnail(name: string): url {
        return Qt.resolvedUrl(assetsBaseDir + "/752x423/" + name);
    }

    function resolveFull(name: string): url {
        return Qt.resolvedUrl(assetsBaseDir + "/1504x846/" + name);
    }
}
