import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15
import QtQml.Models 2.15

// import QtGraphicalEffects 1.15 as GE
import Qt5Compat.GraphicalEffects as GE

import org.kde.kirigami 2.20 as Kirigami

QQC2.AbstractButton {
    id: controlRoot

    required property int index
    required property bool isAnimated
    required property string image
    required property CarouselModel carouselModel

    signal maximize()

    // property real radius: Kirigami.Units.smallSpacing
    property real radius: Kirigami.Units.gridUnit

    readonly property real minimumRatio: 1/2
    readonly property real maximumRatio: 2/1

    readonly property real preferredRatio: {
        if (activeImage.status !== Image.Ready || activeImage.implicitHeight === 0) {
            return 3/2;
        }
        return activeImage.implicitWidth / activeImage.implicitHeight;
    }

    readonly property real ratio: {
        return Math.max(minimumRatio, Math.min(maximumRatio, preferredRatio));
    }

    property bool dim: true

    property bool loadFull: false

    readonly property Image activeImage: loadFull ? imageFullView : imageThumbnainView

    readonly property alias imageFullView: imageFullView
    readonly property alias imageThumbnainView: imageThumbnainView

    width: Math.round(height * ratio) + leftPadding + rightPadding
    height: ListView.view?.height ?? 0

    onWidthChanged: {
        // print("Delegate W", index, width, height);
    }

    onHeightChanged: {
        // print("Delegate H", index, width, height);
    }

    padding: 0
    topPadding: undefined
    leftPadding: undefined
    rightPadding: undefined
    bottomPadding: undefined
    verticalPadding: undefined
    horizontalPadding: Kirigami.Units.smallSpacing

    leftInset: leftPadding
    rightInset: rightPadding

    contentItem: GE.OpacityMask {
        id: content

        implicitWidth: controlRoot.activeImage.implicitWidth
        implicitHeight: controlRoot.activeImage.implicitHeight

        source: Item {
            width: content.width
            height: content.height

            Rectangle {
                anchors.fill: parent
                z: 0

                // color: controlRoot.Kirigami.Theme.backgroundColor
                // color: controlRoot.Kirigami.Theme.textColor
                color: "gray"

                QQC2.BusyIndicator {
                    anchors.centerIn: parent
                    running: controlRoot.activeImage.status === Image.Loading
                }

                Kirigami.Icon {
                    anchors.centerIn: parent
                    implicitWidth: Kirigami.Units.iconSizes.large
                    implicitHeight: Kirigami.Units.iconSizes.large
                    visible: controlRoot.activeImage.status === Image.Error
                    source: "image-missing"
                }
            }

            Image {
                id: imageThumbnainView

                anchors.fill: parent
                z: 1

                cache: true
                fillMode: Image.PreserveAspectFit
                source: controlRoot.image && (!controlRoot.loadFull || imageFullView.status !== Image.Ready)
                    ? controlRoot.carouselModel.resolveThumbnail(controlRoot.image) : ""
            }

            Image {
                id: imageFullView

                anchors.fill: parent
                z: 2

                cache: true
                fillMode: Image.PreserveAspectFit
                source: controlRoot.image && controlRoot.loadFull
                    ? controlRoot.carouselModel.resolveFull(controlRoot.image) : ""
            }
        }

        maskSource: Rectangle {
            width: content.width
            height: content.height

            color: "black"
            radius: controlRoot.radius
        }

        opacity: (!dim || controlRoot.ListView.isCurrentItem) ? 1 : controlRoot.hovered ? 0.8 : 0.66

        Behavior on opacity {
            NumberAnimation {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutCubic
            }
        }
    }

    background: Kirigami.ShadowedRectangle {
        color: "transparent"
        radius: controlRoot.radius

        shadow.size: 20
        shadow.xOffset: 0
        shadow.yOffset: 0
        shadow.color: Qt.rgba(0, 0, 0, 0.6)
    }

    onClicked: {
        if (ListView.view) {
            if (ListView.view.currentIndex === index) {
                controlRoot.maximize();
            } else {
                ListView.view.currentIndex = index;
            }
        }
    }
}
