import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQml 2.15

import org.kde.kirigami 2.20 as Kirigami

QtObject {
    id: root

    required property CarouselModel carouselModel
    required property ListView inlineView

    enum Host {
        FullScreen,
        Overlay
    }

    property /*CarouselMaximizedHostController.Host*/ int preferredHost: CarouselMaximizedHostController.Host.FullScreen

    readonly property /*CarouselMaximizedHostController.Host*/ int effectiveHost: Kirigami.Settings.isMobile
        ? CarouselMaximizedHostController.Host.Overlay : preferredHost

    readonly property list<Component> __hosts: [
        Component {
            id: fullScreenComponent

            CarouselFullScreenWindowMaximizedHost { }
        },
        Component {
            id: overlayComponent

            CarouselOverlayMaximizedHost { }
        }
    ]

    function getHostComponentForEffectiveMode(): Component {
        switch (effectiveHost) {
        case CarouselMaximizedHostController.Host.FullScreen:
            return fullScreenComponent;
        case CarouselMaximizedHostController.Host.Overlay:
        default:
            return overlayComponent;
        }
    }

    property CarouselAbstractMaximizedHost __host: null

    function open(index: int) {
        if (__host) {
            __host.currentIndex = index;
            return;
        }

        const hostComponent = getHostComponentForEffectiveMode();
        __host = hostComponent.createObject(this, {
            carouselModel,
            inlineView,
        });
        __host.closed.connect(() => __host.destroy());
        __host.currentIndex = index;
        __host.open();
    }
}
