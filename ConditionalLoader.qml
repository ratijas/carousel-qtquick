import QtQuick 2.1
import QtQuick.Layouts 1.1

Loader {
    property Component componentTrue
    property Component componentFalse
    property bool condition

    Layout.minimumHeight: item ? item.Layout.minimumHeight : 0
    Layout.minimumWidth: item ? item.Layout.minimumWidth : 0
    sourceComponent: condition ? componentTrue : componentFalse
}
